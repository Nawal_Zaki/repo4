﻿using System;
using B2BRateQuote;
using static B2BRateQuote.RateQuoteServiceSoapClient;
using System.Xml;

namespace RLLibrary
{
    class RLLibrary
    {
        EndpointConfiguration endpointConfiguration;
        RateQuoteServiceSoapClient rateQuoteServiceSoapClient;
        RateQuoteRequest rateQuoteRequest;
        Item item;
        string APIKey = "YtM2MThzE3M1JiNjMtMWNhNi00MDA5LWEzMjZTQ0MkN2NzQjC";
        string MyRLCID = "";

        public RLLibrary()
        {
            endpointConfiguration = new EndpointConfiguration();
            rateQuoteServiceSoapClient = new RateQuoteServiceSoapClient(endpointConfiguration);
            rateQuoteRequest = new RateQuoteRequest();
            rateQuoteRequest.Origin = new ServicePoint();
            rateQuoteRequest.Destination = new ServicePoint();
            item = new Item();

        }
        public string GetRateQuote(ShipmentDetails shipmentDetails)
        {
            rateQuoteRequest.Origin.ZipOrPostalCode = shipmentDetails.originZipCode;
            rateQuoteRequest.Origin.CountryCode = shipmentDetails.originCountryCode;
            rateQuoteRequest.Destination.CountryCode = shipmentDetails.destinationCountryCode;
            rateQuoteRequest.Destination.ZipOrPostalCode = shipmentDetails.destinationZipCode;
            item.Class = shipmentDetails.itemClass;
            item.Weight = shipmentDetails.weight;
            Item[] items = { item };
            rateQuoteRequest.Items = items;
            var response = rateQuoteServiceSoapClient.GetRateQuoteAsync(APIKey, rateQuoteRequest);
            string customerData = response.Result.CustomerData;
            bool wasSuccess = response.Result.WasSuccess;
            return $"customerData: {customerData}| wasSuccess:{wasSuccess}";
        }
        public PalletType[] GetPalletTypes()
        {
            var response = rateQuoteServiceSoapClient.GetPalletTypesAsync(APIKey);
            PalletType[] responseList = response.Result.Result;
            foreach (var i in responseList)
            {
                Console.WriteLine(i.ToString());
            }
            return responseList;
        }

        public string GetPalletTypeByPoints(ShipmentDetails shipmentDetails)
        {
            rateQuoteRequest.Origin.ZipOrPostalCode = shipmentDetails.originZipCode;
            rateQuoteRequest.Origin.City = shipmentDetails.originCity;
            rateQuoteRequest.Destination.City = shipmentDetails.destinationCity;
            rateQuoteRequest.Destination.ZipOrPostalCode = shipmentDetails.destinationZipCode;
            var reponse = rateQuoteServiceSoapClient.GetPalletTypeByPointsAsync(APIKey, MyRLCID, rateQuoteRequest.Origin.City, rateQuoteRequest.Origin.ZipOrPostalCode, rateQuoteRequest.Destination.City, rateQuoteRequest.Destination.ZipOrPostalCode);
            return reponse.Result.Result.ToString();

        }

    }
}
