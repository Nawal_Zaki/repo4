﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RLLibrary
{
    class ShipmentDetails
    {
        public string originZipCode;
        public string originCountryCode;
        public string destinationZipCode;
        public string originCity;
        public string destinationCity;
        public string destinationCountryCode;
        public string itemClass;
        public float weight;

        public ShipmentDetails(string originZipCode, string originCountryCode, string destinationZipCode, string destinationCountryCode, string destinationCity, string originCity, string itemClass, float weight)
        {
            this.originZipCode = originZipCode;
            this.originCountryCode = originCountryCode;
            this.originCity = originCity;
            this.destinationZipCode = destinationZipCode;
            this.destinationCountryCode = destinationCountryCode;
            this.destinationCity = destinationCity;
            this.itemClass = itemClass;
            this.weight = weight;
        }
    }
}
