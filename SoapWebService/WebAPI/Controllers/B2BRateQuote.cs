﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using B2BRateQuote;
using Microsoft.AspNetCore.Mvc;
using RLLibrary;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class B2BRateQuote : ControllerBase
    {
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("GetRateQuote")]
        public async Task<JsonResult> GetRate()
        {
            RLLibrary consumer = new RLLibrary();
            ShipmentDetails s = new ShipmentDetails("eg", "eg", "eg", "eg", "30.0", 300.0f);
            string response = consumer.GetRateQuote(s);

            return Json(new Response(HttpStatusCode.OK)
            {
                Message = "done"
            });
        }

    }
}
